---
title: EVENTNAME Hackathon Agreement
organizer:
  name: "[ORGANIZER NAME]"
  description: "[ORGANIZER DESCRIPTION]"
sponsors:
  - name: "[SPONSOR A NAME]"
    description: "[SPONSOR A DESCRIPTION]"
  - name: "[SPONSOR B NAME]"
    description: "[SPONSOR B DESCRIPTION]"
eligibility:
 - "above the age of majority in the country, state, province or jurisdiction of
 residence at the time of entry; and"
 - "not a person or entity under U.S. export controls or sanctions."
registration:
  deadline:
    time: HH:MM
    date: MONTH DAY, YEAR
    timezone: TIMEZONE 
  email: registration@example.com
  location: "[SOME PLACE]"
demonstration:
  date: MONTH DAY, YEAR
period:
  start:
    time: HH:MM
    date: MONTH DAY, YEAR
    timezone: TIMEZONE 
  end:
    time: HH:MM
    date: MONTH DAY, YEAR
    timezone: TIMEZONE
criteria: "[SUCH CRITERIA AS APPROPRIATE]"
judging:
  relationship: industry professionals appointed by the Hackathon Organizer
  date: "[SOME DATE]"
reward:
  pool: "[SOME SUM]"
  criteria: "[SOME CRITERIA]"
  sum: "[SOME SUM]"
privacy:
  policy: "https://example.com"
  email: "privacy@example.com"
  foreign: "including the United States"
jurisdiction: "[JURISDICTION]"
arbitration:
  board: "[A DESIGNATED ARBITRATION ORGANIZATION]"
  location: "[DESIGNATED ARBITRATION LOCATION]"
display:
  rewardList: false
---

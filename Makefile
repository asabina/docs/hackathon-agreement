clean:
	rm *.html

%.html: hackathon.md style.css
	rm $@; \
		pandoc $(@:.html=.md) --standalone --template hackathon.md | \
		pandoc -t html5 --standalone --css style.css | \
		tee $@

.PHONY: clean

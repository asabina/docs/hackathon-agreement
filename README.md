# Hackathon Agreement

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>

> The text of the Agreement is derived from the _Model Hackathon Terms_
> licensed by Google, Inc. under CC-BY-4.0, published on
> https://opensource.google.com/docs/hackathons/#modelterms and accessed on
> January 23rd, 2019. The license pertains to the copy of the Agreement and has
> been parameterized to allow substitution of the placeholders for the content
> applicable to a particular case.

# Usage

Generate a HTML payload representing the styled Agreement by running `make
example-event.html` on a system that has GNU Make and pandoc installed to
produce the example-event.html file.
